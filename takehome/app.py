from flask import Flask, request, jsonify,url_for,redirect,session

import json
import uuid
from flask import render_template

from tinydb import TinyDB, Query, where

dbo = TinyDB('users_db.json')

app = Flask(__name__)
app.secret_key = "abc"

@app.route("/")
def index():

    asd =  dbo.search(where('properties'))
    data_str = ""
    data_str = str(asd)
    data = json.loads(data_str.replace("'", '"'))
    asda = asd["properties"]
    print( str(asd["properties"]))
    return ""


@app.route('/test', methods=['GET'])
def tst():
    x = uuid.uuid4()

    dbo.insert({'user_id': str(x), 'username': 'ecepektas', 'password': 'ecepektasasdas', 'properties': [], 'bookings': []})
    User = Query()
    count = dbo.count((where('username') == 'ecepektas')& (where('password') == 'asdsdsfsd' ))
    a =dbo.all()
    print(a)
    print(count)

    return 'test'


@app.route('/register', methods=['POST'])
def registerPost():
    # request
    x = uuid.uuid4()
    username = request.form['username']
    password = request.form['password']

    dbo.insert( {"user_id": str(x), "username": username, "password": password, "properties": [], "bookings": []})

    return 'user_id is --> ' + str(x)

@app.route('/register', methods=['GET'])
def registerGet():
    # request

   #return render_template("public/register.html",user='username')
    return render_template("public/register.html")


@app.route('/login', methods=['GET'])
def loginGet():
    return render_template("public/index.html")

@app.route('/logout', methods=['GET'])
def logout():
    session['user_id'] = ""
    return render_template("public/index.html")

@app.route('/login', methods=['POST'])
def loginPost():


    username = request.form['username']
    password = request.form['password']

    print(username)
    print(password)
    count = dbo.count((where('username') == username) & (where('password') == password))


    if count > 0 :
        user = dbo.get(where('username') == username)
        data_str = ""
        data_str = str(user)
        data = json.loads(data_str.replace("'",'"'))
        print (data['user_id'])
        session['user_id'] = data['user_id']
        return 'user_id is --> ' + data['user_id']
    else:
        return 'Username or Password invalid.'


@app.route('/properties', methods=['GET'])
def properties():
    return "You have reached to properties page"


@app.route('/property', methods=['POST'])
def property():
    # request
    ...


@app.route('/property/create', methods=['POST'])
def property_create():
    x = uuid.uuid4()
    user_id_ses = session['user_id']

    numberOfBedrooms = request.form['numberOfBedrooms']
    numberOfRooms= request.form['numberOfRooms']
    name= request.form['name']

    user = dbo.get(where('user_id') == user_id_ses)
    data_str = ""
    data_str = str(user)
    data = json.loads(data_str.replace("'", '"'))

    properties = {"property_id":str(x), "name": name, "numberOfRooms": int(numberOfRooms), "numberOfBedrooms": int(numberOfBedrooms)}

    sa = data['properties'].append(properties)

    print(data['properties'])

    dbo.update({'properties': data['properties']}, where('user_id') == user_id_ses)

    print (dbo.all())
    return ""

@app.route('/property/create', methods=['GET'])
def property_createGET():

    user_id_ses = session['user_id']
    user_count = dbo.count((where('user_id') == user_id_ses))
    if user_count > 0:
        return render_template("public/create_property.html")
    else:
        return render_template("public/404.html")

@app.route('/property/update', methods=['POST'])
def property_update():
    ...


@app.route('/property/delete', methods=['DELETE'])
def property_delete():
    # request
    ...


if __name__ == '__main__':
    app.run()
